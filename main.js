var request = require('request-json'),
    GeoJSON = require('geojson'),
    UUID = require('node-uuid'),
    _ = require('lodash');

var HOST = process.env.CUSTOMCONNSTR_HOST || 'http://httpbin.org';
var ENDPOINT = process.env.CUSTOMCONNSTR_ENDPOINT || 'post';
var LOCATION_LATITUDE = process.env.LOCATION_LATITUDE || 50.2945;
var LOCATION_LONGITUDE = process.env.LOCATION_LONGITUDE || 18.6714;
var REQS_SEC = process.env.REQS_SEC || 100;
var DEBUG = process.env.DEBUG || false;

var CITIES = {
  "Adelaide, Australia": {latitude: -34.916667, longitude: 138.6},
  "Berlin, Germany": {latitude: 52.518611, longitude: 13.408056},
  "Cologne, Germany": {latitude: 50.938056, longitude: 6.956944},
  "Chicago, IL": {latitude: 41.881944, longitude: -87.627778},
  "Dallas, TX": {latitude: 32.783056, longitude: -96.806667},
  "Denver, CO": {latitude: 39.778889, longitude: -104.9825},
  "Hamburg, Germany": {latitude: 53.550556, longitude: 9.993333},
  "Las Vegas, NV": {latitude: 36.1168, longitude: -115.173798},
  "Los Angeles, CA": {latitude: 34.052222, longitude: -118.243611},
  "Melbourne, Australia": {latitude: -37.8, longitude: 144.95},
  "Mexico City, Mexico": {latitude: 19.419444, longitude: -99.145556},
  "Moscow, Russia": {latitude: 55.751667, longitude: 37.617778},
  "Munich, Germany": {latitude: 48.137222, longitude: 11.575556},
  "New York City, NY": {latitude: 40.715517, longitude: -73.9991},
  "Oslo, Norway": {latitude: 59.91, longitude: 10.75},
  "Phoenix, AZ": {latitude: 33.448083, longitude: -112.073083},
  "San Francisco, CA": {latitude: 37.774514, longitude: -122.418079},
  "Stockholm, Sweden": {latitude: 59.325, longitude: 18.05},
  "Sydney, Australia": {latitude: -33.85, longitude: 151.2},
  "Toronto, Canada": {latitude: 43.66135, longitude: -79.383087},
  "Vancouver, Canada": {latitude: 49.28098, longitude: -123.12244},
  "Washington, D.C.": {latitude: 38.895, longitude: -77.036667},
  "Gliwice, Poland": {latitude: 50.2945, longitude: 18.6714}
};

function oscillateAround(location) {

}

function valueFunction(val, seed) {
  return Math.abs(Math.sin(val) * seed);
}

function* nextValue() {
  let series = _.range(0, Math.PI, 0.05);

  let current = 0;
  while (true) {
    yield series[current];

    current++;

    if (current > series.length) current = 0;
  }
}

var nval = nextValue();
var seed = Math.random() * 100;

function newValue() {
  return valueFunction(nval.next().value, seed);
}

function location() {
    return {
      longitude: LOCATION_LONGITUDE,
      latitude: LOCATION_LATITUDE
    };
}

function newInsert() {
  return {
    value: newValue(),
    geo: location()
  };
}

function newRequest() {
  var req_id = UUID.v4();
  console.time('request ' + req_id);
  let client = request.createClient(HOST);

  client.post(ENDPOINT, newInsert(), function(e, r, body) {
    if (e) {
      console.error(e);
    }

    if (DEBUG) console.log(body);

    console.timeEnd('request ' + req_id);
  });
}

setInterval(newRequest, REQS_SEC);
